import { Cart, Offers, ProcessedProduct } from "./types";

// TODO: Replace this with the actual type
type State = unknown;

export function initializeOffers(offers: Offers): State {
	return null;
}

export function processProducts(state: State, cart: Cart): ProcessedProduct[] {
	return [];
}
